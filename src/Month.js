import React, { useState } from 'react';
import Day from './Day.js';

function Month(props){

  const [month, setMonth] = useState('1');

  const monthChange = (event) => {
    setMonth(event.target.value);
  };

  return (
    <>
      <select name="month" id="month" onChange={monthChange}>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
      </select>

      <Day month={month} />
    </>
    )
  };
  export default Month;
  