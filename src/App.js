import logo from './logo.svg';
import './App.css';
import Month from './Month.js';

function App() {
  return (
    <div className="App">
      <Month />
    </div>
  );
}

export default App;
