function Day(props){

    const date = new Date("2021-" + props.month + "-01 00:00:00");
    let day = date.toLocaleDateString('en-US', { weekday: 'long' });;

    return <div>Month {props.month}/2021 starts {day}</div>;
  };
  export default Day;
  